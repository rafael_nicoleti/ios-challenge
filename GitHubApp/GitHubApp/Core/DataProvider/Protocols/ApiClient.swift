//
//  ApiClient.swift
//  GitHubApp
//
//  Created by Rafael Nicoleti on 22/02/17.
//  Copyright © 2017 Rafael Nicoleti. All rights reserved.
//

import Foundation

enum APIError: Error {
    case unknown
    case http(Error)
    case adapter
}

protocol ApiClient {
    func repositories(by page: Int, completion: @escaping (Result<[Repository], APIError>) -> ())
    
}
