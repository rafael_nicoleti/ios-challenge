//
//  DataProvider.swift
//  GitHubApp
//
//  Created by Rafael Nicoleti on 22/02/17.
//  Copyright © 2017 Rafael Nicoleti. All rights reserved.
//

import Foundation
import RxSwift

final class DataProvider {
    
    private let client: ApiClient
    
    init(client: ApiClient) {
        self.client = client
    }
    
    var error = Variable<Error?>(nil)

    func repositories(by page: Int) -> [Repository] {
        var resultRepositories: [Repository] = []
        
        client.repositories(by: page) { [weak self] result in
            switch result {
            case .success(let repos):
                resultRepositories = repos
            case .error(let error):
                self?.error.value = error
            }
        }
    
        return resultRepositories
    }
}

