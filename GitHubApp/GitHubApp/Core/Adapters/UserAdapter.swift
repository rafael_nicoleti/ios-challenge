//
//  UserAdapter.swift
//  GitHubApp
//
//  Created by Rafael Nicoleti on 22/02/17.
//  Copyright © 2017 Rafael Nicoleti. All rights reserved.
//

import Foundation
import SwiftyJSON

private extension User {
    struct Keys {
        static let id = "id"
        static let login = "login"
        static let email = "email"
        static let name = "name"
        static let avatar = "avatar_url"
    }
}

final class UserAdapter: Adapter<JSON, User> {
    
    override func adapt() -> Result<User, AdapterError> {
        let id = input[User.Keys.id].stringValue
        let login = input[User.Keys.login].stringValue
        let avatar = input[User.Keys.avatar].stringValue
        
        // the fields id, login and avatar are the minimum required to consider the user valid
        guard !id.isEmpty && !login.isEmpty && !avatar.isEmpty else {
            return .error(.missingRequiredFields)
        }
        
        let user = User(
            id: id,
            login: login,
            email: input[User.Keys.email].string,
            name: input[User.Keys.name].string,
            avatar: avatar
        )
        
        return .success(user)
    }
    
}

