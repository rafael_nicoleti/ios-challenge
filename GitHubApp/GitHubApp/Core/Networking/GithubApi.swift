//
//  GithubApi.swift
//  GitHubApp
//
//  Created by Rafael Nicoleti on 22/02/17.
//  Copyright © 2017 Rafael Nicoleti. All rights reserved.
//

import Foundation
import Siesta
import SwiftyJSON

final class GithubAPI {
    
    private struct Endpoints {
        static let repositories = "/search/repositories/"
    }
    
    private let service = Service(baseURL: "https://api.github.com")
    
    init() {
        service.configure("**") { config in
            
            config.headers["Authorization"] = self.authenticationHeader
            
            config.pipeline[.parsing].add(self.jsonParser, contentTypes: ["*/json"])
            
            config.pipeline[.cleanup].add(GithubErrorHandler())
        }
        
        
        service.configureTransformer(Endpoints.repositories) {
            try self.failableAdapt(using: RepositoryAdapter(input: $0.content as JSON))
        }
    }
    
    func repositories(by page: Int) -> Resource {
        return service
            .resource(Endpoints.repositories)
            .withParam("q", "language:Swift")
            .withParam("sort", "stars")
            .withParam("page", "\(page)")
    }
    
    
    private lazy var authenticationHeader: String = {
        guard let auth = "\(Credentials.username):\(Credentials.accessToken)".data(using: String.Encoding.utf8) else {
            fatalError("Unable to generate authentication header, check username and accessToken in Credentials.swift")
        }
        
        return "Basic \(auth.base64EncodedString())"
    }()
    
    private let jsonParser = ResponseContentTransformer { JSON($0.content as AnyObject) }
    
    private struct GithubErrorHandler: ResponseTransformer {
        
        func process(_ response: Response) -> Response {
            switch response {
            case .success:
                return response
            case .failure(var error):
                // update error with Github error
                error.userMessage = error.jsonDict["message"] as? String ?? error.userMessage
                return .failure(error)
            }
        }
        
    }
    
    private func failableAdapt<T>(using adapter: Adapter<JSON, T>) throws -> T {
        let result = adapter.adapt()
        switch result {
        case .success(let entity):
            return entity
        case .error(let error):
            throw error
        }
    }
    
}

