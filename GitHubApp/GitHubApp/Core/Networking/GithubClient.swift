//
//  GithubClient.swift
//  GitHubApp
//
//  Created by Rafael Nicoleti on 22/02/17.
//  Copyright © 2017 Rafael Nicoleti. All rights reserved.
//


import Foundation
import Siesta

extension Siesta.Resource {
    
    var error: APIError {
        if let underlyingError = self.latestError {
            return .http(underlyingError)
        } else {
            return .unknown
        }
    }
    
}

final class GithubClient: ApiClient {
    
    private let concreteClient = GithubAPI()
    
    func repositories(by page: Int, completion: @escaping (Result<[Repository], APIError>) -> ()) {
        concreteClient.repositories(by: page).addObserver(owner: self) { [weak self] resource, event in
            self?.process(resource, event: event, with: completion)
        }.loadIfNeeded()
    }
    
    private func process<M>(_ resource: Siesta.Resource, event: Siesta.ResourceEvent, with completion: @escaping (Result<M, APIError>) -> ()) {
        switch event {
        case .error:
            completion(.error(resource.error))
        case .newData(_), .notModified:
            print(resource.typedContent()!)
            if let results: M = resource.typedContent() {
                completion(.success(results))
            } else {
                completion(.error(.adapter))
            }
        default: break
        }
    }
}
