//
//  Repository.swift
//  GitHubApp
//
//  Created by Rafael Nicoleti on 22/02/17.
//  Copyright © 2017 Rafael Nicoleti. All rights reserved.
//

import Foundation

struct Repository {
    
    let id: String
    let name: String
    let fullName: String
    let description: String
    let stars: Int
    let forks: Int
    let owner: User?
}

extension Repository: Equatable { }

func ==(lhs: Repository, rhs: Repository) -> Bool {
    return lhs.id == rhs.id
        && lhs.name == rhs.name
        && lhs.fullName == rhs.fullName
        && lhs.description == rhs.description
        && lhs.stars == rhs.stars
        && lhs.forks == rhs.forks
        && lhs.owner == rhs.owner
}
